package mcp.mobius.mobiuscore;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.util.Arrays;

/**
 * Created by Weissmoon on 4/1/19.
 */
@Mod(modid = "mobiuscore", name = "MobiusCore", version = "1.2.5", acceptableRemoteVersions = "*")
public class DummyCore {

    @Mod.EventHandler
    public void preInit (FMLPreInitializationEvent event) {

        ModMetadata md = event.getModMetadata();
        md.modId = "mobiuscore";
        md.name = "MobiusCore";
        md.version = "1.2.5";
        md.credits = "ProfMobius";
        md.authorList = Arrays.asList("ProfMobius");
        md.description = "";
        md.url = "profmobius.blogspot.com";
    }
}
