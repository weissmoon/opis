package mcp.mobius.opis.commands;

import mcp.mobius.opis.commands.IOpisCommand;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

/**
 * Created by Weissmoon on 4/1/19.
 */
public abstract class OpisCommnandBase extends CommandBase implements IOpisCommand {

    /**IOpisCommand Begin*/
    @Override
    public abstract String getDescription();
    /**Name Begin*/
    @Override
    public String getCommandNameOpis() {
        return this.getCommandName();
    }
    /**IOpisCommand End*/
    @Override
    public String getName() {
        return this.getCommandName();
    }
    public abstract String getCommandName();
    /**Name End*/

    /**Usage Begin*/
    @Override
    public String getUsage(ICommandSender sender) {
        return this.getCommandUsage(sender);
    }

    public abstract String getCommandUsage(ICommandSender icommandsender);
    /**Usage End*/


    /**Process Begin*/
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        this.processCommand(sender, args);
    }

    public abstract void processCommand(ICommandSender icommandsender, String[] astring);
    /**Process End*/


    /**Permission Begin*/
    public boolean checkPermission(MinecraftServer server, ICommandSender sender){
        return this.canCommandSenderUseCommand(sender);
    }

    public abstract boolean canCommandSenderUseCommand(ICommandSender sender);
    /**Permission End*/
}
