package mcp.mobius.opis.commands.server;

import mcp.mobius.opis.commands.OpisCommnandBase;
import mcp.mobius.opis.modOpis;
import mcp.mobius.opis.commands.IOpisCommand;
import mcp.mobius.opis.events.PlayerTracker;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.dedicated.DedicatedServer;
import mcp.mobius.mobiuscore.profiler.ProfilerSection;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.command.TextComponentHelper;

public class CommandStop extends OpisCommnandBase implements IOpisCommand {

	@Override
	public String getCommandName() {
		return "opis_stop";
	}

	@Override
	public String getCommandNameOpis() {
		return this.getCommandName();
	}	
	
	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "";
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring) {
		modOpis.profilerRun = false;
		ProfilerSection.desactivateAll(Side.SERVER);
		icommandsender.sendMessage(TextComponentHelper.createComponentTranslation(icommandsender, String.format("\u00A7oOpis stopped.")));
	}

	@Override
    public int getRequiredPermissionLevel()
    {
        return 3;
    }	

	@Override
    public boolean canCommandSenderUseCommand(ICommandSender sender)
    {
		if (sender instanceof DedicatedServer) return true;
		//if ((sender instanceof EntityPlayerMP) && ((EntityPlayerMP)sender).playerNetServerHandler.netManager instanceof MemoryConnection) return true;
		if (!(sender instanceof DedicatedServer) && !(sender instanceof EntityPlayerMP)) return true;
		return PlayerTracker.INSTANCE.isPrivileged(((EntityPlayerMP)sender).getGameProfile().getName());
    }

	@Override
	public String getDescription() {
		return "Ends a run before completion.";
	}	
	
}
