package mcp.mobius.opis.commands.server;

import java.util.HashMap;
import java.util.Set;

import mcp.mobius.opis.commands.OpisCommnandBase;
import net.minecraft.util.math.ChunkPos;
import org.apache.logging.log4j.Level;

import mcp.mobius.opis.modOpis;
import mcp.mobius.opis.commands.IOpisCommand;
import net.minecraft.command.ICommandSender;
import net.minecraftforge.common.DimensionManager;

public class CommandChunkDump extends OpisCommnandBase implements IOpisCommand{

	@Override
	public String getCommandName() {
		return "chunkdump";
	}

	@Override
	public String getCommandNameOpis() {
		return this.getCommandName();
	}	
	
	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "";
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring) {
		modOpis.log.log(Level.INFO, "== CHUNK DUMP ==");
		
		HashMap<ChunkPos, Boolean> chunkStatus = new HashMap<ChunkPos, Boolean>();
		
		Integer[] worldIDs = DimensionManager.getIDs();
		for (Integer worldID : worldIDs){
			Set<ChunkPos> persistantChunks = DimensionManager.getWorld(worldID).getPersistentChunks().keySet();
			Set<ChunkPos> chunks = (Set<ChunkPos>)DimensionManager.getWorld(worldID).getPlayerChunkMap();
			//Set<ChunkPos> chunks = (Set<ChunkPos>)DimensionManager.getWorld(worldID).activeChunkSet;
			
			for (ChunkPos chunk : chunks){
				modOpis.log.log(Level.INFO, String.format("Dim : %s, %s, Forced : %s", worldID, chunk, persistantChunks.contains(chunk)));
				chunkStatus.put(chunk, persistantChunks.contains(chunk));
			}
		}
		
		//((EntityPlayerMP)icommandsender).playerNetServerHandler.sendPacketToPlayer(Packet_LoadedChunks.create(chunkStatus));
	}

	@Override
    public int getRequiredPermissionLevel()
    {
        return 0;
    }	

	@Override
    public boolean canCommandSenderUseCommand(ICommandSender par1ICommandSender)
    {
        return false;
    }

	@Override
	public String getDescription() {
		return "Unused";
	}	
	
}
